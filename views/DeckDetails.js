import React, {Component} from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native'
import { AppLoading } from 'expo'
import iconPlus from '../src/icon_plus.png'

import utils from '../helpers/utils'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  titleContainer: {
    flex: 1,
    justifyContent: 'center'
  },

  title: {
    fontSize: 32,
    alignItems: 'center',
    textAlign: 'center'
  },

  textSubtitle: {
    fontSize: 12,
    marginTop: 25,
    alignItems: 'center',
    textAlign: 'center'
  },

  btn: {
    backgroundColor: '#00ced1',
    paddingHorizontal: 20,
    paddingVertical: 10,
    margin: 5,
    width: 250,
    alignItems: 'center'
  },

  btnText: {
    color: 'white'
  },

  btnAdd: {
    backgroundColor: '#00ced1',
    width: 70,
    height: 70,
    borderRadius: 35,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 10,
    bottom: 10
  },
})

export default class DeckDetails extends Component {
  state = {
    deck: {},
    ready: false
  }
  static navigationOptions = ({ navigation }) => ({
    title: `${navigation.state.params.title} details`,
  })

  componentDidMount() {
    this.updateData()
  }

  startQuiz() {
    this.props.navigation.navigate('Quiz', {id: this.state.deck.id})
  }

  updateData(booble) {
    utils.getDeck(this.props.navigation.state.params.id).then(
      deck => {
        this.setState({deck, ready: true})
        if (booble) {
          this.props.navigation.state.params.updater()
        }
      }
    )
  }

  addCard() {
    this.props.navigation.navigate('AddCard', {id: this.state.deck.id, updater: (booble) => this.updateData(booble)})
  }

  render() {
    const { deck, ready } = this.state

    if (!ready) {
      return <AppLoading/>
    }

    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{this.props.navigation.state.params.title}</Text>
          <Text style={styles.textSubtitle}>
            {deck.questions.length} card(s)
          </Text> 
        </View>  
        
        <View style={{flex: 1}}>
          <TouchableOpacity
            style={styles.btn}
            onPress={() => this.startQuiz()}
          >
            <Text style={styles.btnText}>Start quiz</Text>
          </TouchableOpacity>
        </View>  
        <TouchableOpacity
          style={styles.btnAdd}
          onPress={() => this.addCard()}
        >
          <Image source={iconPlus} />  
        </TouchableOpacity>
      </View>
    )
  }
}
