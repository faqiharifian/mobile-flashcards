import React, {Component} from 'react'
import { Text, View, TouchableOpacity, StyleSheet, TextInput } from 'react-native'
import { AppLoading } from 'expo'

import utils from '../helpers/utils'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  btn: {
    backgroundColor: '#00ced1',
    paddingHorizontal: 20,
    paddingVertical: 10,
    margin: 5,
    width: 250,
    alignItems: 'center'
  },

  btnText: {
    color: 'white'
  }
})

export default class DeckDetails extends Component {
  state = {
    question: '',
    answer: ''
  }
  static navigationOptions = ({ navigation }) => ({
    title: 'Add new card',
  })

  saveCard() {
    utils.saveCard(
      this.props.navigation.state.params.id,
      this.state.question,
      this.state.answer
    ).then(
      () => {
        this.props.navigation.state.params.updater(true)
      }
    )
    this.props.navigation.goBack()
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Question:</Text>  
        <View style={{ flexDirection: 'row', margin: 5 }}>
          <TextInput
            style={{flex:0.8, borderWidth:1, height:30, padding: 5}}
            onChangeText={(question) => this.setState({question})}
            value={this.state.question}
            placeholder='Insert the question'
            placeholderTextColor='rgb(50,50,50)'
          />
        </View>
        <Text>Answer:</Text>  
        <View style={{ flexDirection: 'row', margin: 5 }}>
          <TextInput
            style={{flex:0.8, borderWidth:1, height:30, padding: 5}}
            onChangeText={(answer) => this.setState({answer})}
            value={this.state.answer}
            placeholder='Insert the answer'
            placeholderTextColor='rgb(50,50,50)'
          />
        </View>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => this.saveCard()}
        >
          <Text style={styles.btnText}>Submit</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
