import React, {Component} from 'react'
import { Text, View, TouchableOpacity, StyleSheet, TextInput } from 'react-native'
import { AppLoading } from 'expo'

import { NavigationActions } from 'react-navigation'

import utils from '../helpers/utils'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  textHint: {
    fontSize: 24,
    paddingHorizontal: 10,
    marginHorizontal: 30,
    textAlign: 'center'
  },

  btn: {
    backgroundColor: '#00ced1',
    paddingHorizontal: 20,
    paddingVertical: 10,
    margin: 5,
    width: 250,
    alignItems: 'center'
  },

  btnText: {
    color: 'white'
  }
})

export default class DeckDetails extends Component {
  state = {
    name: ''
  }
  static navigationOptions = ({ navigation }) => ({
    title: 'Add new deck',
  })

  saveDeck() {
    utils.saveDeck(
      this.state.name
    ).then(
      ({title, id}) => {
        this.props.navigation.state.params.updater()
        this.props.navigation.goBack()
        this.props.navigation.navigate(
          'DeckDetails',
          {title, id, updater: () => this.props.navigation.state.params.updater()}
        )
      }
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.textHint}>What is the title of your new deck?</Text>  
        <View style={{flexDirection:'row', margin: 5}}>
          <TextInput
            style={{flex:0.8, borderWidth:1, height:30, padding: 5}}
            onChangeText={(name) => this.setState({name})}
            value={this.state.name}
          />
        </View>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => this.saveDeck()}
        >
          <Text style={styles.btnText}>Submit</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
