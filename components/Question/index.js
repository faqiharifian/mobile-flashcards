import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 0.7,
    alignItems: 'center',
    justifyContent: 'center'
  },

  btnText: {
    color: '#b61827'
  },

  text: {
    fontSize: 20
  }
})

export default class Question extends Component {
  state = {
    toogle: false
  }

  componentWillReceiveProps() {
    this.setState({
      toogle: false
    })
  }

  render () {
    const { question, answer } = this.props

    return (
      <View style={styles.container}>
        {!this.state.toogle && (
          <Text style={styles.text}>
            {question}
          </Text>
        )}
        {this.state.toogle && (
          <Text style={styles.text}>
            {answer}
          </Text>
        )}

        <TouchableOpacity
          onPress={() => this.setState({toogle: !this.state.toogle})}
        >
          {!this.state.toogle && (
              <Text style={styles.btnText}>Show answer</Text>
          )}
          {this.state.toogle && (
            <Text style={styles.btnText}>Show question</Text>
          )}
        </TouchableOpacity>
      </View>
    )
  }
}
