import React from 'react'
import { Text, View, TouchableOpacity, Animated, StyleSheet } from 'react-native'

import { withNavigation } from 'react-navigation';

const styles = StyleSheet.create({
  container: {
    margin: 10,
    paddingVertical: 10,
    paddingHorizontal: 5,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    elevation: 3
  }
})

const Deck = ({id, title, number, updater, navigation}) => (
  <View>
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        Animated.sequence([
          Animated.timing(new Animated.Value(1), { duration: 200, toValue: 1.04 }),
          Animated.spring(new Animated.Value(1), { toValue: 1, friction: 4 })
        ]).start((finished) => { 
          navigation.navigate('DeckDetails', { title, id, updater })
        })
        
      }}
    >
      <Text>{title}</Text>
      <Text>{number || 0} card(s)</Text>
    </TouchableOpacity>
  </View>
)

export default withNavigation(Deck)
