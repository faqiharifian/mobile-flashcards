# Udacicards  


## Installation  

```bash  
yarn install  
yarn start  
```

## Usage  

To view the app with live reloading, point the Expo app to the QR code.  
You'll find the QR scanner on the Projects tab of the app.  

Or enter the address in the Expo app's search bar:  

- Press `a` to open Android device or emulator.
- Press `q` to display QR code.
- Press `r` to restart packager, or `R` to restart packager and clear cache.
- Press `d` to toggle development mode.

## License  
[MIT License](https://opensource.org/licenses/MIT)
